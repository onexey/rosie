﻿using System;
using System.Linq;
using Xunit;

namespace Rosie.Tests.Data
{
    public class RosieContextTest
    {
        [Fact]
        public void TelegramUsers_Insert_Success()
        {
            var rand = new Random();
            var firstName = $"firstName_{rand.Next()}";
            var lastName = $"lastName_{rand.Next()}";
            var id = rand.Next();
            var username = $"username_{rand.Next()}";
            var language = $"language_{rand.Next()}";
            var isAllowed = rand.Next(0, 1) == 1;

            var user = new Models.TelegramUser()
            {
                FirstName = firstName,
                LastName = lastName,
                UserId = id,
                Username = username,
                IsAllowed = isAllowed,
                Language = language
            };

            using var db = new Rosie.Data.RosieContext();
            db.TelegramUsers.Add(user);
            db.SaveChanges();

            var dbUsr = db.TelegramUsers.Where(x => x.UserId == id).FirstOrDefault();

            Assert.True(dbUsr != null &&
                dbUsr.FirstName == firstName &&
                dbUsr.LastName == lastName &&
                dbUsr.Username == username &&
                dbUsr.Language == language &&
                dbUsr.IsAllowed == isAllowed);
        }
    }
}
