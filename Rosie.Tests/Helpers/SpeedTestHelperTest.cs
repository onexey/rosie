using System;
using Xunit;

namespace Rosie.Tests.Helpers
{
    public class SpeedTestHelperTest
    {
        [Fact]
        public void RunSpeedTest_NonWhiteSpace()
        {
            var test = Rosie.Helpers.SpeedTestHelper.RunSpeedTest();

            Assert.False(string.IsNullOrWhiteSpace(test.Result));
        }
    }
}
