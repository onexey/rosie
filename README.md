# Rosie

This is my implementation of home automation. The name `Rosie` (`Rosie the Robot Maid`) comes from my favorite childhood cartoon `The Jetsons`.

## What I did till now

- Created a basic `gRPC` implementation based on built in `.NET` template using `.NET Core 3.1`. <sub>2020-05-25</sub>
- Completed `Auth0` integration for authentication & authorization. <sub>2020-05-25</sub>
- Added a basic client to consume/test server. <sub>2020-05-25</sub>
- Added a new project for `Arduino` named ArduinoNode. <sub>2020-05-27</sub>
  - Today I received my ethernet shield for `Arduino UNO`. My main goal was make them talk with server over `gRPC`. Because I wanted to use subscription model of `gRPC`. Unfortunately I discovered my shield won't support TLS. So I cannot make them talk without disabling encryption, which I don't want to. Opening `Rosie` to the internet still an option so I need that.
  - Instead, I converted it to a `REST` server. Rosie will connect and get/post data with `REST`. Used [aREST](https://github.com/marcoschwartz/aREST) library.
  - Added DHT11 sensor for the first prototype. Used [DHT sensor library](https://github.com/adafruit/DHT-sensor-library). <sub>2020-05-28</sub>
- Added `Telegram` implementation. <sub>2020-06-06</sub>
  - Used a BackgroundService for that. This will change in future updates. Done. It's now a static class. <sub>2020-06-06</sub>
- `SQLite` implementation completed. <sub>2020-06-06</sub>
  - I may change it entity framework. Right now it's a basic implementation. <sub>2020-06-07</sub>
    - Done.
  - ~~On second thought, I can still change it to a noSQL engine. I want to use a new technology.~~ I won't.
- Internet speed tester. <sub>2020-06-25</sub>
  - Found a [library](https://github.com/JoyMoe/SpeedTest.Net) that uses speedtest.net.
- Added first System.Reactive sample for scheduled jobs. <sub>2020-06-25</sub>
- Added first unit test project and a few test cases. <sub>2020-06-28</sub>

## What I want to add to this project

- `gRPC` subscription implementation so I can push information to clients.
- `Samsung SmartThings` implementation so can manage my TV remotely.
