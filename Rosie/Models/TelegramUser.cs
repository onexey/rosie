﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Rosie.Models
{
    public class TelegramUser
    {
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Is Allowed?")]
        public bool IsAllowed { get; set; } = false;

        [Display(Name = nameof(Language))]
        public string Language { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Key]
        [Display(Name = "Telegram User ID")]
        public int UserId { get; set; }

        [Display(Name = nameof(Username))]
        public string Username { get; set; }
    }
}
