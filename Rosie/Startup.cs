﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Rosie.Data;
using Rosie.Services;
using System;
using System.Linq;

namespace Rosie
{
    public class Startup
    {
        public Startup(IConfiguration configuration) { Configuration = configuration; }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if(env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            } else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }


            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<RosieMainService>();
                endpoints.MapRazorPages();

                // Uncomment to enable REST support.
                // endpoints.MapControllers();

                // endpoints.MapGet("/", async context =>
                // {
                //     await context.Response
                //         .WriteAsync("Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");
                // });
            });

            var serverLifetime = app.ApplicationServices.GetService(typeof(IHostApplicationLifetime)) as IHostApplicationLifetime;
            serverLifetime.ApplicationStarted
                .Register(() =>
            {
                // If this is first run, open configuration page.
                if(Common.Config.FirstRun)
                {
                    var adr = app.ServerFeatures.Get<IServerAddressesFeature>().Addresses.First();
                    adr = adr.Replace("//0.0.0.0", "//localhost");
                    adr = adr.Replace("//[::]", "//localhost");

                    Helpers.Utils.OpenBrowser(adr);
                }
            });
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            // Add Authentication Services
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(options =>
                {
                    options.Authority = $"https://{Common.Config.Auth0.Domain}/";
                    options.Audience = Common.Config.Auth0.Audience;
                });

            // Add Authorization Services
            services.AddAuthorization();

            // Add Application Itself
            services.AddGrpc();
            services.AddRazorPages()
                .AddMvcOptions(options =>
                {
                    options.Filters.Add(new Filters.RazorAsyncPageFilter(Configuration));
                    options.Filters.Add(new Filters.RazorPageFilter(Configuration));
                });

            services.AddDbContext<RosieContext>(); 

            // Add Rest Controllers
            // services.AddControllers();
        }
    }
}