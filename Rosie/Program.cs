using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Hosting;
using System;
using System.Linq;
using System.Runtime.InteropServices;

namespace Rosie
{
    public class Program
    {
        private static Services.SchedulerService _Scheduler = new Services.SchedulerService();

        // Additional configuration is required to successfully run gRPC on macOS.
        // For instructions on how to configure Kestrel and gRPC clients on macOS, visit https://go.microsoft.com/fwlink/?linkid=2099682
        public static IHostBuilder CreateHostBuilder(string[] args) => Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                if(RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                {
                    // If macOS use HTTP/1.1 endpoint without TLS.
                    // https://docs.microsoft.com/en-us/aspnet/core/grpc/troubleshoot?view=aspnetcore-3.1#unable-to-start-aspnet-core-grpc-app-on-macos

                    webBuilder.ConfigureKestrel(options =>
                    {
                        options.ListenAnyIP(5001,
                              listenOptions =>
                              {
                                  listenOptions.Protocols = HttpProtocols.Http1;
                              });
                    });
                }

                webBuilder.UseStartup<Startup>();
            });

        public static void Main(string[] args)
        {
            AppDomain.CurrentDomain.ProcessExit += CurrentDomain_ProcessExit;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            Helpers.TelegramHelper.Start();

            CreateHostBuilder(args).Build().Run();
        }

        private static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            _Scheduler.Dispose();
            Helpers.TelegramHelper.Stop();
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        { Console.WriteLine(e.ExceptionObject.ToString()); }
    }
}
