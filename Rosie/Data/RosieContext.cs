using Microsoft.EntityFrameworkCore;
using Rosie.Models;

namespace Rosie.Data
{
    public class RosieContext : DbContext
    {
        public RosieContext()
        {
            ChangeTracker.LazyLoadingEnabled = false;
            Database.Migrate();
        }

        public RosieContext(DbContextOptions<RosieContext> options) : base(options)
        {
        }

        public DbSet<TelegramUser> TelegramUsers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options) => options.UseSqlite($"Data Source={System.IO.Path.Combine(Common.BaseDirectory, "AppData.db")}");

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TelegramUser>().ToTable(nameof(TelegramUser));
            modelBuilder.Entity<TelegramUser>().HasKey(c => new { c.UserId });
        }
    }
}