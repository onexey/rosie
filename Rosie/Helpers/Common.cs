using Rosie.Helpers;
using System;
using System.IO;

public static class Common
{
    public static string BaseDirectory = System.AppDomain.CurrentDomain.BaseDirectory;
    private static readonly string _ConfigFile = "Rosie.config";
    private static RosieConfiguration _Config = default;

    public static RosieConfiguration Config
    {
        get
        {
            if(_Config == null)
            {
                try
                {
                    lock(_ConfigFile)
                    {
                        if(File.Exists(Path.Combine(BaseDirectory, _ConfigFile)))
                        {
                            var fileContent = File.ReadAllText(Path.Combine(BaseDirectory, _ConfigFile));
                            var conf = new RosieConfiguration(fileContent);
                            _Config = conf;
                        }
                    }
                } catch(Exception ex)
                {
                }
            }

            return _Config ?? new RosieConfiguration();
        }

        private set { _Config = value; }
    }

    public static bool SaveConfiguration()
    { return SaveToFile(Path.Combine(BaseDirectory, _ConfigFile), Config.ToString()); }

    public static bool SaveConfiguration(RosieConfiguration config)
    {
        Config = config;
        return SaveConfiguration();
    }

    public static bool SaveToFile(string path, string content)
    {
        var isSuccess = false;
        if(!string.IsNullOrWhiteSpace(path))
        {
            var dirName = Path.GetDirectoryName(path);
            if(!string.IsNullOrWhiteSpace(dirName) && !Directory.Exists(dirName))
                Directory.CreateDirectory(dirName);

            File.WriteAllText(path, content);
            isSuccess = true;
        }

        return isSuccess;
    }
}