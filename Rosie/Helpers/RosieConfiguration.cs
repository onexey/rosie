﻿using Newtonsoft.Json;
using System;
using System.Linq;

namespace Rosie.Helpers
{
    public class Auth0Config
    {
        public Auth0Config()
        {
        }

        public string Audience { get; set; } = string.Empty;

        public string Domain { get; set; } = string.Empty;

        public override string ToString() { return JsonConvert.SerializeObject(this, Formatting.Indented); }
    }

    public class ProxyConfiguration
    {
        public ProxyConfiguration()
        {
        }

        public string Host { get; set; }

        public int Port { get; set; }

        public override string ToString() { return JsonConvert.SerializeObject(this, Formatting.Indented); }
    }

    public class RosieConfiguration
    {
        public RosieConfiguration()
        {
        }

        public RosieConfiguration(string configData)
        {
            var conf = JsonConvert.DeserializeObject<RosieConfiguration>(configData);
            foreach(var item in conf.GetType().GetProperties())
            {
                GetType().GetProperty(item.Name).SetValue(this, item.GetValue(conf));
            }
        }

        public Auth0Config Auth0 { get; set; } = new Auth0Config();

        public bool FirstRun { get; set; } = true;

        public string OwnerMail { get; set; } = default;

        public string OwnerName { get; set; } = default;

        public ProxyConfiguration Proxy { get; set; } = new ProxyConfiguration();

        public TelegramBotConfiguration TelegramBot { get; set; } = new TelegramBotConfiguration();

        public bool UseProxy { get; set; } = false;

        public override string ToString() { return JsonConvert.SerializeObject(this, Formatting.Indented); }
    }

    public class TelegramBotConfiguration
    {
        public TelegramBotConfiguration()
        {
        }

        public string Token { get; set; } = string.Empty;

        public override string ToString() { return JsonConvert.SerializeObject(this, Formatting.Indented); }
    }
}
