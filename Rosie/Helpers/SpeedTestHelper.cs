﻿using SpeedTest;
using SpeedTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rosie.Helpers
{
    public static class SpeedTestHelper
    {
        private static SpeedTestClient client;
        private static Settings settings;

        public static async Task<string> RunSpeedTest()
        {
            var result = string.Empty;

            await Task.Run(() =>
            {
                try
                {
                    Console.WriteLine("Getting speedtest.net settings and server list...");
                    client = new SpeedTestClient();
                    settings = client.GetSettings();

                    var servers = SelectServers();
                    var bestServer = SelectBestServer(servers);

                    Console.WriteLine("Testing speed...");
                    var downloadSpeed = client.TestDownloadSpeed(bestServer, settings.Download.ThreadsPerUrl);
                    PrintSpeed("Download", downloadSpeed);
                    var uploadSpeed = client.TestUploadSpeed(bestServer, settings.Upload.ThreadsPerUrl);
                    PrintSpeed("Upload", uploadSpeed);

                    result += $"SpeedTest Results:{Environment.NewLine}";
                    result += $"Download: {GetPrettySpeed(downloadSpeed)}{Environment.NewLine}";
                    result += $"Upload: {GetPrettySpeed(uploadSpeed)}{Environment.NewLine}";
                    result += $"Hosted by {bestServer.Sponsor} ({bestServer.Name}/{bestServer.Country}), distance: {(int)bestServer.Distance / 1000}km, latency: {bestServer.Latency}ms";
                } catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            });

            return result;
        }

        private static string GetPrettySpeed(double speed)
        {
            string result;
            if(speed > 1024 * 1024)
            {
                result = $"{(Math.Round(speed / (1024 * 1024), 2))} Gbps";
            } else if(speed > 1024)
            {
                result = $"{(Math.Round(speed / 1024, 2))} Mbps";
            } else
            {
                result = $"{(Math.Round(speed, 2))} Kbps";
            }

            return result;
        }

        private static void PrintServerDetails(Server server)
        {
            Console.WriteLine($"Hosted by {server.Sponsor} ({server.Name}/{server.Country}), distance: {(int)server.Distance / 1000}km, latency: {server.Latency}ms");
        }

        private static void PrintSpeed(string type, double speed)
        {
            Console.WriteLine($"{type} speed: {GetPrettySpeed(speed)}");
        }

        private static Server SelectBestServer(IEnumerable<Server> servers)
        {
            Console.WriteLine();
            Console.WriteLine("Best server by latency:");
            var bestServer = servers.OrderBy(x => x.Latency).First();
            PrintServerDetails(bestServer);
            Console.WriteLine();
            return bestServer;
        }

        private static IEnumerable<Server> SelectServers()
        {
            Console.WriteLine();
            Console.WriteLine("Selecting best server by distance...");
            var servers = settings.Servers.Take(10).ToList();

            foreach(var server in servers)
            {
                server.Latency = client.TestServerLatency(server);
                // PrintServerDetails(server);
            }
            return servers;
        }
    }
}
