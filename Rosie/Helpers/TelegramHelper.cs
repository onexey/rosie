using Rosie.Data;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InlineQueryResults;
using Telegram.Bot.Types.InputFiles;
using Telegram.Bot.Types.ReplyMarkups;

namespace Rosie.Helpers
{
    public static class TelegramHelper
    {
        private static TelegramBotClient Bot;

        public static async Task SendMessageToAdmin(Task<string> message) { await SendMessageToAdmin(message.Result); }

        public static async Task SendMessageToAdmin(string message)
        {
            using var db = new RosieContext();
            var usrList = db.TelegramUsers.Where(x => x.IsAllowed);

            if(usrList != null)
            {
                foreach(var item in usrList)
                {
                    await Bot.SendTextMessageAsync(chatId: item.UserId, text: message, replyMarkup: new ReplyKeyboardRemove());
                }
            }
        }

        public static async void SendUsage(int chatId)
        {
            var chat = new Chat() { Id = chatId };
            var msg = new Message() { Chat = chat };

            await Usage(msg);
        }

        public static void Start()
        {
            if(string.IsNullOrWhiteSpace(Common.Config.TelegramBot.Token))
                return;

            if(Common.Config.UseProxy)
            {
                var Proxy = new WebProxy(Common.Config.Proxy.Host, Common.Config.Proxy.Port)
                { UseDefaultCredentials = true };
                Bot = new TelegramBotClient(Common.Config.TelegramBot.Token, webProxy: Proxy);
            } else
            {
                Bot = new TelegramBotClient(Common.Config.TelegramBot.Token);
            }

            var me = Bot.GetMeAsync();

            Bot.OnMessage += BotOnMessageReceived;
            Bot.OnMessageEdited += BotOnMessageReceived;
            Bot.OnCallbackQuery += BotOnCallbackQueryReceived;
            Bot.OnInlineQuery += BotOnInlineQueryReceived;
            Bot.OnInlineResultChosen += BotOnChosenInlineResultReceived;
            Bot.OnReceiveError += BotOnReceiveError;

            Bot.StartReceiving(Array.Empty<UpdateType>());

            SendStartupMessageAsync();

            Console.WriteLine($"Start listening for @{me.Result.Username}");
        }

        public static void Stop()
        {
            if(Bot != null)
            {
                System.Console.WriteLine("Telegram Bot is shutting down...");
                SendShutDownMessage();
                Bot.StopReceiving();
            }
        }

        // Process Inline Keyboard callback data
        private static async void BotOnCallbackQueryReceived(object sender, CallbackQueryEventArgs callbackQueryEventArgs)
        {
            var callbackQuery = callbackQueryEventArgs.CallbackQuery;

            await Bot.AnswerCallbackQueryAsync(
                callbackQueryId: callbackQuery.Id,
                text: $"Received {callbackQuery.Data}");

            await Bot.SendTextMessageAsync(
                chatId: callbackQuery.Message.Chat.Id,
                text: $"Received {callbackQuery.Data}");
        }

        private static async void BotOnMessageReceived(object sender, MessageEventArgs messageEventArgs)
        {
            var message = messageEventArgs.Message;

            if(message == null || message.Type != MessageType.Text)
                return;

            var sndr = Newtonsoft.Json.JsonConvert.SerializeObject(message.From);
            var cht = Newtonsoft.Json.JsonConvert.SerializeObject(message.Chat);
            Console.WriteLine($"Message Received From: {sndr}");
            Console.WriteLine($"Message Received: {cht}");

            if(UserAllowed(message.From))
            {
                switch(message.Text.Split(' ').First())
                {
                    // Send inline keyboard
                    case "/inline":
                        await SendInlineKeyboard(message);
                        break;

                    // send custom keyboard
                    case "/keyboard":
                        await SendReplyKeyboard(message);
                        break;

                    // send a photo
                    case "/photo":
                        await SendDocument(message);
                        break;

                    // request location or contact
                    case "/request":
                        await RequestContactAndLocation(message);
                        break;

                    // Send inline keyboard
                    case "/speedtest":
                        RunSpeedTest(message);
                        break;

                    default:
                        await Usage(message);
                        break;
                }
            } else
            {
                await SendMessageAsync(message.Chat.Id, "I don't know you!");
            }
        }

        private static void BotOnReceiveError(object sender, ReceiveErrorEventArgs receiveErrorEventArgs)
        {
            Console.WriteLine($"Received error: {receiveErrorEventArgs.ApiRequestException.ErrorCode} — {receiveErrorEventArgs.ApiRequestException.Message}");
        }

        private static async Task RequestContactAndLocation(Message msg)
        {
            var RequestReplyKeyboard = new ReplyKeyboardMarkup(new[]
                { KeyboardButton.WithRequestLocation("Location"), KeyboardButton.WithRequestContact("Contact"), });
            await Bot.SendTextMessageAsync(
                chatId: msg.Chat.Id,
                text: "Who or Where are you?",
                replyMarkup: RequestReplyKeyboard);
        }

        private static async void RunSpeedTest(Message msg)
        {
            await Task.Run(async() =>
            {
                var test = SpeedTestHelper.RunSpeedTest();
                await SendMessageAsync(msg.Chat.Id, test.Result);
            });
        }

        private static async Task SendDocument(Message msg)
        {
            await Bot.SendChatActionAsync(msg.Chat.Id, ChatAction.UploadPhoto);

            const string filePath = @"Files/tux.png";
            using var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            var fileName = filePath.Split(Path.DirectorySeparatorChar).Last();
            await Bot.SendPhotoAsync(
                chatId: msg.Chat.Id,
                photo: new InputOnlineFile(fileStream, fileName),
                caption: "Nice Picture");
        }

        // Send inline keyboard
        // You can process responses in BotOnCallbackQueryReceived handler
        private static async Task SendInlineKeyboard(Message msg)
        {
            await Bot.SendChatActionAsync(msg.Chat.Id, ChatAction.Typing);

            // Simulate longer running task
            // await Task.Delay(500);

            var inlineKeyboard = new InlineKeyboardMarkup(new[]
                {
                    // first row
                    new []
                    {
                        InlineKeyboardButton.WithCallbackData("1.1", "11"),
                        InlineKeyboardButton.WithCallbackData("1.2", "12"),
                    },
                    // second row
                    new []
                    {
                        InlineKeyboardButton.WithCallbackData("2.1", "21"),
                        InlineKeyboardButton.WithCallbackData("2.2", "22"),
                    }
                });
            await Bot.SendTextMessageAsync(
                chatId: msg.Chat.Id,
                text: "Choose",
                replyMarkup: inlineKeyboard);
        }

        private static void SendMessage(long chatId, string message)
        { Bot.SendTextMessageAsync(chatId: chatId, text: message, replyMarkup: new ReplyKeyboardRemove()); }

        private static async Task SendMessageAsync(long chatId, string message)
        { await Bot.SendTextMessageAsync(chatId: chatId, text: message, replyMarkup: new ReplyKeyboardRemove()); }

        private static async Task SendReplyKeyboard(Message msg)
        {
            var replyKeyboardMarkup = new ReplyKeyboardMarkup(
                new KeyboardButton[][]
                { new KeyboardButton[] { "1.1", "1.2" }, new KeyboardButton[] { "2.1", "2.2" }, },
                resizeKeyboard: true);

            await Bot.SendTextMessageAsync(
                chatId: msg.Chat.Id,
                text: "Choose",
                replyMarkup: replyKeyboardMarkup);
        }

        private static void SendShutDownMessage()
        {
            const string msg = "Goodbye Cruel World!";

            using var db = new RosieContext();
            var usrList = db.TelegramUsers.Where(x => x.IsAllowed);

            if(usrList != null)
            {
                foreach(var item in usrList)
                {
                    SendMessage(item.UserId, msg);
                }
            }
        }

        private static async void SendStartupMessageAsync()
        {
            const string msg = "I'm Home!";

            using var db = new RosieContext();
            var usrList = db.TelegramUsers.Where(x => x.IsAllowed);

            if(usrList != null)
            {
                foreach(var item in usrList)
                {
                    await SendMessageAsync(item.UserId, msg);
                }
            }
        }

        private static async Task Usage(Message msg)
        {
            const string usage = "Usage:\n"
                + "/inline   - send inline keyboard\n"
                + "/keyboard - send custom keyboard\n"
                + "/photo    - send a photo\n"
                + "/request  - request location or contact";
            await Bot.SendTextMessageAsync(
                chatId: msg.Chat.Id,
                text: usage,
                replyMarkup: new ReplyKeyboardRemove());
        }

        private static bool UserAllowed(User from)
        {
            var allowed = false;

            using var db = new RosieContext();
            var dbUsr = db.TelegramUsers.Where(x => x.UserId == from.Id).FirstOrDefault();

            if(dbUsr == null)
            {
                // Create
                Console.WriteLine("Inserting a new user");
                var usr = new Models.TelegramUser
                {
                    UserId = from.Id,
                    FirstName = from.FirstName,
                    LastName = from.LastName,
                    Username = from.Username,
                    Language = from.LanguageCode
                };
                db.TelegramUsers.Add(usr);
                db.SaveChanges();
            } else
            {
                allowed = dbUsr.IsAllowed;
            }


            return allowed;
        }

        #region Inline Mode

        private static async void BotOnInlineQueryReceived(object sender, InlineQueryEventArgs inlineQueryEventArgs)
        {
            Console.WriteLine($"Received inline query from: {inlineQueryEventArgs.InlineQuery.From.Id}");

            InlineQueryResultBase[] results =
            {
                // displayed result
                new InlineQueryResultArticle(
                    id: "3",
                    title: "TgBots",
                    inputMessageContent: new InputTextMessageContent(
                        "hello"))
            };
            await Bot.AnswerInlineQueryAsync(inlineQueryId: inlineQueryEventArgs.InlineQuery.Id, results: results, isPersonal: true, cacheTime: 0);
        }

        private static void BotOnChosenInlineResultReceived(object sender, ChosenInlineResultEventArgs chosenInlineResultEventArgs)
        {
            Console.WriteLine($"Received inline result: {chosenInlineResultEventArgs.ChosenInlineResult.ResultId}");
        }

        #endregion
    }
}