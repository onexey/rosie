﻿using Grpc.Core;
using Microsoft.AspNetCore.Authorization;
using Rosie.Protos;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Rosie.Services
{
    public class RosieMainService : RosieMain.RosieMainBase
    {
        [Authorize]
        public override Task<NotifyReply> NotifyAdmin(Notify request, ServerCallContext context)
        {
            //var user = context.GetHttpContext().User;

            //// requester id
            //var sss = user.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;

            return Task.FromResult(new NotifyReply
                { Success = true, Message = $"Done! You wrote {request.Message}. Priority was {request.Priority}" });
        }

        public override Task<NotifyReply> Status(Request request, ServerCallContext context)
        { return Task.FromResult(new NotifyReply { Success = true, Message = "All systems are operational." }); }
    }
}
