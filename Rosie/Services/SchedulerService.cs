﻿using System;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;

namespace Rosie.Services
{
    public class SchedulerService : IDisposable
    {
        private bool disposedValue;
        private IDisposable subscription;

        public SchedulerService()
        {
            var observable = from i in Observable.Interval(TimeSpan.FromMinutes(1))
                             from t in Observable.FromAsync(() => SchedulerHit())
                select t;

            subscription = observable.Subscribe();
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~SchedulerService()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if(!disposedValue)
            {
                if(disposing)
                {
                    // TODO: dispose managed state (managed objects)

                    subscription.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        private static async Task SchedulerHit()
        {
            await Task.Run(() =>
            {/* Speed Test */
                if(DateTime.Now.Hour == 7 && DateTime.Now.Minute == 30)
                {
                    var test = Helpers.SpeedTestHelper.RunSpeedTest();
                    Helpers.TelegramHelper.SendMessageToAdmin(test).Wait();
                }
            });
        }
    }
}
