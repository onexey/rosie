using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Rosie.Pages
{
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public class FirstRunModel : PageModel
    {
        private readonly ILogger<FirstRunModel> _logger;

        public FirstRunModel(ILogger<FirstRunModel> logger) { _logger = logger; }

        [BindProperty]
        public Helpers.RosieConfiguration SiteConfig { get; set; }

        public void OnGet()
        {
            SiteConfig = Common.Config;
        }

        public void OnPost()
        {
            if (ModelState.IsValid)
            {
                SiteConfig.FirstRun = false;

                Common.SaveConfiguration(SiteConfig);
            }
        }
    }
}
