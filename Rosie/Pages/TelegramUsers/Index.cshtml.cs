﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Rosie.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rosie.Pages.TelegramUsers
{
    public class IndexModel : PageModel
    {
        private readonly RosieContext _context;
        private readonly ILogger<IndexModel> _logger;

        public IndexModel(ILogger<IndexModel> logger, RosieContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IList<Models.TelegramUser> Users { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            Users = await _context.TelegramUsers.ToListAsync();

            return Page();
        }
    }
}