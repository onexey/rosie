﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Rosie.Data;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Rosie.Pages.TelegramUsers
{
    public class EditModel : PageModel
    {
        private readonly RosieContext _context;
        private readonly ILogger<EditModel> _logger;

        public EditModel(ILogger<EditModel> logger, RosieContext context)
        {
            _logger = logger;
            _context = context;
        }

        [BindProperty]
        public Models.TelegramUser TelegramUser { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if(id == null)
            {
                return NotFound();
            }

            TelegramUser = await _context.TelegramUsers.FindAsync(id);

            if(TelegramUser == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int id)
        {
            var userToUpdate = await _context.TelegramUsers.FindAsync(id);

            if(userToUpdate == null)
            {
                return NotFound();
            }

            if(await TryUpdateModelAsync(userToUpdate, nameof(TelegramUser), s => s.IsAllowed, s => s.LastName, s => s.FirstName, s => s.Language, s => s.Username))
            {
                await _context.SaveChangesAsync();
                return RedirectToPage("./Index");
            }

            return Page();
        }
    }
}