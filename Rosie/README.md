# Journey

From now on, I'll update here about the changes made on Rosie(server).

This project more like a guinea pig for me. I want to try new things in here and see what problems I will face. So there will be a lot of desing changes during development.

## Database

This part a bit complicated. I'll try to summarise it.

Decided to use SQLite with my own implemantation in a DatabaseHelper class. Because I did this before and I know how.

Later on, I wanted to try another DB engine so looked for embedded NoSQL implementations for a while. litedb was the most likely choice at the time. At this point I want to use DB for storing data and configuration for the services.

Before changing to NoSQL, I decided to give another shot to SQLite. But this time with EntityFramework. For the application configuration, I used a json formatted file instead.

And [here](https://docs.microsoft.com/en-us/ef/core/providers/sqlite/limitations#migrations-limitations-workaround) is why a good research important before using a technology. At first I couldn't manage the update schema but I didn't dwell on it. Decided that is another days problem to fix. That day is today. It turns out, SQLite doesn't support dropping columns. Which is not bueno. I'm not expecting randomly droping a column, but the idea of inability to doing so bothers me. I don't know another good embedded DBMS and have to do a little research before proceeding.
