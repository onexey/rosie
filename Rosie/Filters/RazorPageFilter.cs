using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;

namespace Rosie.Filters
{
    public class RazorPageFilter : IPageFilter
    {
        private readonly IConfiguration _config;

        public RazorPageFilter(IConfiguration config) { _config = config; }

        public void OnPageHandlerExecuted(PageHandlerExecutedContext context)
        {
            // OnPageHandlerExecuted : Called after the handler method executes, before the action result.
        }

        public void OnPageHandlerExecuting(PageHandlerExecutingContext context)
        {
            // OnPageHandlerExecuting : Called before the handler method executes, after model binding is complete.
        }

        public void OnPageHandlerSelected(PageHandlerSelectedContext context)
        {
            // OnPageHandlerSelected : Called after a handler method has been selected, but before model binding occurs.

            if(Common.Config.FirstRun)
            {
                if(context.HttpContext.Request.Path != "/FirstRun")
                {
                    var response = context.HttpContext.Response;
                    response.Redirect("/FirstRun");
                }
            }
        }
    }
}