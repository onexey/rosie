using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace Rosie.Filters
{
    public class RazorAsyncPageFilter : IAsyncPageFilter
    {
        private readonly IConfiguration _config;

        public RazorAsyncPageFilter(IConfiguration config) { _config = config; }

        public async Task OnPageHandlerExecutionAsync(PageHandlerExecutingContext context, PageHandlerExecutionDelegate next)
        {
            // OnPageHandlerExecutionAsync: Called asynchronously before the handler method is invoked, after model binding is complete.

            await next.Invoke();
        }

        public Task OnPageHandlerSelectionAsync(PageHandlerSelectedContext context)
        {
            // OnPageHandlerSelectionAsync : Called asynchronously after the handler method has been selected, but before model binding occurs.

            return Task.CompletedTask;
        }
    }
}