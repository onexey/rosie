﻿using Newtonsoft.Json;
using System;

namespace RosieClient.Helpers
{
    public class Auth0Config
    {
        public Auth0Config()
        {
        }

        public string Audience { get; set; } = string.Empty;

        public string ClientId { get; set; } = string.Empty;

        public string ClientSecret { get; set; } = string.Empty;

        public string Domain { get; set; } = string.Empty;

        public override string ToString() { return JsonConvert.SerializeObject(this, Formatting.Indented); }
    }

    public class Rosie
    {
        public Rosie()
        {
        }

        public string ServerAddress { get; set; } = string.Empty;

        public override string ToString() { return JsonConvert.SerializeObject(this, Formatting.Indented); }
    }

    public class RosieClientConfiguration
    {
        public RosieClientConfiguration()
        {
        }

        public RosieClientConfiguration(string configData)
        {
            var conf = JsonConvert.DeserializeObject<RosieClientConfiguration>(configData);
            foreach(var item in conf.GetType().GetProperties())
            {
                GetType().GetProperty(item.Name).SetValue(this, item.GetValue(conf));
            }
        }

        public Auth0Config Auth0 { get; set; } = new Auth0Config();

        public Rosie Rosie { get; set; } = new Rosie();

        public override string ToString() { return JsonConvert.SerializeObject(this, Formatting.Indented); }
    }
}
