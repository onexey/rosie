﻿
using Auth0.AuthenticationApi;
using Auth0.AuthenticationApi.Models;
using Grpc.Core;
using Grpc.Net.Client;
using RosieClient.Helpers;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RosieClient
{
    public static class Common
    {
        public static readonly string _GrpcServerAddress = "https://localhost:5001";
        public static string BaseDirectory = System.AppDomain.CurrentDomain.BaseDirectory;
        private static readonly string _ConfigFile = "RosieClient.config";
        private static RosieClientConfiguration _Config = default;

        public static RosieClientConfiguration Config
        {
            get
            {
                lock(_ConfigFile)
                {
                    if(_Config == null)
                    {
                        try
                        {
                            if(File.Exists(Path.Combine(BaseDirectory, _ConfigFile)))
                            {
                                var fileContent = File.ReadAllText(Path.Combine(BaseDirectory, _ConfigFile));
                                var conf = new RosieClientConfiguration(fileContent);
                                _Config = conf ?? new RosieClientConfiguration();
                            }
                        } catch(Exception ex)
                        {
                        }
                    }
                }

                return _Config ?? new RosieClientConfiguration();
            }

            private set { _Config = value; }
        }

        public static async Task<string> GetAccessToken()
        {
            var auth0Client = new AuthenticationApiClient(Config.Auth0.Domain);
            var tokenRequest = new ClientCredentialsTokenRequest()
            {
                ClientId = Config.Auth0.ClientId,
                ClientSecret = Config.Auth0.ClientSecret,
                Audience = Config.Auth0.Audience
            };
            var tokenResponse = await auth0Client.GetTokenAsync(tokenRequest);

            return tokenResponse.AccessToken;
        }

        public static async Task<Metadata> GetAuthorizationHeader()
        {
            var accessToken = await GetAccessToken();
            var headers = new Metadata { { "Authorization", $"Bearer {accessToken}" } };

            return headers;
        }

        public static bool SaveConfiguration()
        { return SaveToFile(Path.Combine(BaseDirectory, _ConfigFile), Config.ToString()); }

        public static bool SaveConfiguration(RosieClientConfiguration config)
        {
            Config = config;
            return SaveConfiguration();
        }

        public static bool SaveToFile(string path, string content)
        {
            var isSuccess = false;
            if(!string.IsNullOrWhiteSpace(path))
            {
                var dirName = Path.GetDirectoryName(path);
                if(!string.IsNullOrWhiteSpace(dirName) && !Directory.Exists(dirName))
                    Directory.CreateDirectory(dirName);

                File.WriteAllText(path, content);
                isSuccess = true;
            }

            return isSuccess;
        }

        private static async Task<GrpcChannel> CreateAuthorizedChannel()
        {
            var accessToken = await GetAccessToken();

            var credentials = CallCredentials.FromInterceptor((context, metadata) =>
            {
                if(!string.IsNullOrEmpty(accessToken))
                {
                    metadata.Add("Authorization", $"Bearer {accessToken}");
                }
                return Task.CompletedTask;
            });

            var channel = GrpcChannel.ForAddress(Config.Rosie.ServerAddress, new GrpcChannelOptions
                { Credentials = ChannelCredentials.Create(new SslCredentials(), credentials) });
            return channel;
        }

        public static readonly GrpcChannel _ServerChannel = CreateAuthorizedChannel().Result;
    }
}