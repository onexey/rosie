﻿using Rosie.Protos;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RosieClient
{
    public static class CommunicationHandler
    {
        private static RosieMain.RosieMainClient _RosieClient = null;

        public static async Task<NotifyReply> GetStatus()
        {
            InitCommunication();

            return await _RosieClient.StatusAsync(new Request { });
        }

        public static async Task<NotifyReply> NotifyAdmin(string message)
        {
            InitCommunication();

            return await _RosieClient.NotifyAdminAsync(new Notify { Message = message });
        }

        private static void InitCommunication()
        {
            if(_RosieClient == null)
            {
                _RosieClient = new RosieMain.RosieMainClient(Common._ServerChannel);
            }
        }
    }
}