﻿using System;

namespace RosieClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            do
            {
                try
                { 
                    Console.WriteLine();

                    var notify = CommunicationHandler.NotifyAdmin("ZÜleyha");
                    var status = CommunicationHandler.GetStatus();

                    Console.WriteLine($"Reply: \"{notify.Result.Message}\" with is success: {notify.Result.Success}");
                    Console.WriteLine($"status: \"{status.Result.Message}\" with is success: {status.Result.Success}");
                    Console.WriteLine();
                    Console.WriteLine("Press any key to retry. Press \"Q\" to exit.");
                }
                catch (Exception ex)
                {
                    
                }
                finally
                {
                    
                }
            } while (Console.ReadKey().Key != ConsoleKey.Q);
        }
    }
}
